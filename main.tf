// Configure the Google Cloud provider
provider "google" {
 credentials = file("./crypto-resolver-342814-efc0ad13b0a5.json")
 project     = "crypto-resolver-342814"
 region      = "us-central1"
 zone        = "us-central1-a"
}

// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
 byte_length = 8
}

// A single Compute Engine instance
resource "google_compute_instance" "default" {
 name         = "test01-${random_id.instance_id.hex}"
 machine_type = "f1-micro"
 zone         = "us-central1-a"

 boot_disk {
   initialize_params {
     image = "debian-cloud/debian-9"
   }
 }

// Make sure flask is installed on all new instances for later steps
 metadata_startup_script = "touch ~/thisisdoingsomething.sh; sudo apt-get update; sudo apt-get install -y apache2; sudo echo '<p>Created via Terraform into Google Cloud Platform!!</p>' > /var/www/html/index.html;"
 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
}